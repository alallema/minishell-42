/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/27 18:39:50 by alallema          #+#    #+#             */
/*   Updated: 2016/09/17 19:09:22 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H
# include <unistd.h>
# include <stdio.h>
# include <fcntl.h>
# include <sys/wait.h>
# include <signal.h>
# include <dirent.h>
# include <sys/stat.h>
# include "libft.h"
# define SAV (ft_join(ft_strchr_prev(s, ':'), ft_strjoin("/", *av)))
# define LEN ((int)ft_strlen(s) - 1)
# define ENV (ft_strcmp("env", arg[0]) == 0)
# define SET (ft_strcmp("setenv", arg[0]) == 0)
# define EXP (ft_strcmp("export", arg[0]) == 0)
# define UN (ft_strcmp("unset", arg[0]) == 0)
# define UNSET (ft_strcmp("unsetenv", arg[0]) == 0)
# define ECHON (ft_strcmp(arg[1], "-n") != 0 && i != 1)

typedef struct	s_env
{
	char			*path;
	char			*envp;
	struct s_env	*next;
}				t_env;

typedef struct	s_arg
{
	char			**av;
	struct s_arg	*next;
}				t_arg;

char			*ft_join(char *s, char *buf);
void			exec_cmd(t_arg *av, t_env **env);
void			ft_exec(char **av, char *s, t_env *env);
void			ft_chge_env(t_env *env, char *arg, char *oldpwd);
char			**ft_strsplit_sp(char *s);
t_env			*ft_lst_create(char *envp);
void			ft_clear_elem(t_env *elem);
void			ft_lstpushback(t_env **s, char *envp);
void			ft_chg_dir(t_env *env, char **argv);
void			ft_print_lst(t_arg *arg);
void			ft_lst_clear(t_env **begin_list);
t_arg			*ft_splitcmd(char *av);
int				ft_check_built(t_env **env, char **arg);
int				ft_put_env(t_env *env, char **arg);
int				ft_echo(t_env *env, char **arg);
int				ft_setenv(t_env **env, char **arg);
void			ft_clearg(t_arg *av);
void			ft_set_ret(t_env *env, int stat);

#endif
