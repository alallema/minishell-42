/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstarg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 13:05:20 by alallema          #+#    #+#             */
/*   Updated: 2016/09/14 21:30:57 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_free_tab(char **tab)
{
	int		i;

	i = 0;
	if (tab)
	{
		while (tab[i])
		{
			free(tab[i]);
			tab[i] = NULL;
			i++;
		}
		if (tab)
		{
			free(tab);
		}
		tab = NULL;
	}
}

void	ft_clearg(t_arg *arg)
{
	t_arg	*tmp;
	t_arg	*elem;

	elem = arg;
	tmp = NULL;
	while (elem)
	{
		if (elem->next)
			tmp = elem->next;
		else
			tmp = NULL;
		if (elem->av)
		{
			ft_free_tab(elem->av);
			elem->av = NULL;
		}
		free(elem);
		elem = NULL;
		if (tmp)
			elem = tmp;
	}
}

t_arg	*ft_lstarg(char **av)
{
	t_arg	*elem;

	elem = (t_arg *)malloc(sizeof(t_arg));
	if (elem && av)
	{
		elem->av = av;
		elem->next = NULL;
	}
	else
		return (NULL);
	return (elem);
}

void	ft_push_lstarg(t_arg **begin, char *av)
{
	t_arg	*elem;

	elem = *begin;
	if (elem == NULL)
		return ;
	while (elem->next)
		elem = elem->next;
	elem->next = ft_lstarg(ft_strsplit_sp(av));
}

t_arg	*ft_splitcmd(char *av)
{
	t_arg	*lst;
	char	*s;

	s = NULL;
	if (ft_strchr(av, ';'))
	{
		s = ft_strchr_prev(av, ';');
		lst = ft_lstarg(ft_strsplit_sp(s));
		av = ft_strchr(av, ';');
		av++;
		ft_memdel((void *)&s);
		while (av && ft_strchr(av, ';'))
		{
			ft_push_lstarg(&lst, (s = ft_strchr_prev(av, ';')));
			av = ft_strchr(av, ';');
			av++;
			ft_memdel((void *)&s);
		}
		if (av)
			ft_push_lstarg(&lst, av);
	}
	else
		lst = ft_lstarg(ft_strsplit_sp(av));
	return (lst);
}
