/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_create.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/23 12:47:57 by alallema          #+#    #+#             */
/*   Updated: 2016/09/15 18:08:09 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

t_env	*ft_lst_create(char *envp)
{
	t_env	*elem;

	elem = (t_env *)malloc(sizeof(t_env));
	if (elem && envp)
	{
		if (!ft_strchr(envp, '='))
		{
			elem->path = "PWD";
			elem->envp = ft_strdup(envp);
		}
		else
		{
			elem->path = ft_strchr_prev(envp, '=');
			if (ft_strcmp(elem->path, "SHELL") == 0)
				elem->envp = "minishell";
			else if (!(elem->envp = ft_strchr_post(envp, '=')))
				elem->envp = ft_strdup("\0");
		}
		elem->next = NULL;
	}
	else
		return (NULL);
	return (elem);
}

void	ft_lstpushback(t_env **begin, char *envp)
{
	t_env	*elem;
	char	*s;

	s = ft_strchr_prev(envp, '=');
	elem = *begin;
	if (elem == NULL)
	{
		elem = ft_lst_create(envp);
		free(s);
		return ;
	}
	while (elem->next && ft_strcmp(elem->path, s) != 0)
		elem = elem->next;
	if (ft_strcmp(elem->path, s) == 0)
	{
		free(s);
		return ;
	}
	else
		elem->next = ft_lst_create(envp);
	free(s);
}

void	ft_clear_elem(t_env *elem)
{
	t_env	*pre;

	pre = elem;
	elem = elem->next;
	pre->next = elem->next;
	if (*elem->path)
		free(elem->path);
	elem->path = NULL;
	if (*elem->envp)
		free(elem->envp);
	elem->envp = NULL;
	if (elem)
		free(elem);
	elem = NULL;
}

void	ft_lst_clear(t_env **begin_list)
{
	t_env	*tmp;
	t_env	*list;

	list = *begin_list;
	tmp = NULL;
	while (list)
	{
		if (list->next)
			tmp = list->next;
		else
			tmp = NULL;
		free(list);
		list = tmp;
	}
	*begin_list = NULL;
}
