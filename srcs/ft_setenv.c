/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/17 16:55:21 by alallema          #+#    #+#             */
/*   Updated: 2016/09/21 18:16:46 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static int		ck_ag(t_env *env, char *arg, char *arg0)
{
	if (arg && (arg[0] == '=' || !ft_strchr(arg, '=') ||
		(arg[0] > 48 && arg[0] < 57)))
	{
		ft_putstr("minishell: ");
		ft_putendsp(arg0);
		ft_putstr(arg);
		ft_putendl(": not a valid identifier");
		ft_set_ret(env, 1);
		return (-1);
	}
	else
	{
		if (arg && arg[0] != '?')
			ft_set_ret(env, 0);
	}
	return (0);
}

static void		ft_setenv2(t_env *elem, char *arg)
{
	char	*s;

	s = ft_strchr_prev(arg, '=');
	if (*s && ft_strcmp(s, elem->path) == 0)
	{
		if (elem->envp)
			free(elem->envp);
		if (!(elem->envp = ft_strchr_post(arg, '=')))
			elem->envp = ft_strdup("\0");
	}
	if (s)
		free(s);
}

int				ft_setenv(t_env **env, char **arg)
{
	t_env	*elem;
	int		i;

	i = 1;
	elem = *env;
	if (!SET && !EXP)
		return (-1);
	while (arg[i] && arg[i][0] != '-' && (SET || EXP))
	{
		elem = *env;
		while (elem && ft_strchr(arg[i], '='))
		{
			ft_setenv2(elem, arg[i]);
			elem = elem->next;
		}
		if (ck_ag(*env, arg[i], arg[0]) == 0 && !elem && ft_strchr(arg[i], '='))
			ft_lstpushback(env, arg[i]);
		i++;
	}
	return (0);
}
