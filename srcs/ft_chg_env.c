/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chg_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 22:04:33 by alallema          #+#    #+#             */
/*   Updated: 2016/09/14 22:05:26 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void		ft_chg_old(t_env *env, char *oldpwd)
{
	t_env	*elem;

	elem = env;
	if (!oldpwd)
		oldpwd = "";
	while (elem)
	{
		if (ft_strcmp("OLDPWD", elem->path) == 0 && oldpwd)
		{
			free(elem->envp);
			elem->envp = ft_strdup(oldpwd);
		}
		else if (ft_strcmp("OLDPWD", elem->path) == 0 && !oldpwd)
			elem->envp = "\0";
		elem = elem->next;
	}
	return ;
}

void		ft_chge_env(t_env *env, char *arg, char *oldpwd)
{
	t_env	*elem;
	char	buf[256];
	char	*dir;

	elem = env;
	while (arg && arg[0] == '.' && arg[1] == '/')
		arg = arg + 2;
	if (ft_strcmp(arg, "./") == 0 || (arg[0] == '.' && !arg[1]))
		return ;
	while (elem)
	{
		if (ft_strcmp("PWD", elem->path) == 0)
		{
			if (!oldpwd)
				oldpwd = elem->envp;
			free(elem->envp);
			if ((dir = getcwd(buf, 256)))
				elem->envp = ft_strdup(dir);
			else
				elem->envp = ft_strdup("");
		}
		elem = elem->next;
	}
	ft_chg_old(env, oldpwd);
	return ;
}
