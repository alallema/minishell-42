/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/04 15:37:58 by alallema          #+#    #+#             */
/*   Updated: 2016/09/08 20:13:24 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_count_lst(t_env *env)
{
	int			i;
	t_env		*elem;

	i = 0;
	elem = env;
	while (elem)
	{
		elem = elem->next;
		i++;
	}
	return (i);
}

char	**ft_chg_envp(t_env *env)
{
	char		**envp;
	t_env		*elem;
	int			i;
	int			len;

	elem = env;
	i = ft_count_lst(elem);
	envp = (char **)malloc(sizeof(char *) * (i + 1));
	i = 0;
	while (elem)
	{
		len = ft_strlen(elem->path) + ft_strlen(elem->envp) + 1;
		envp[i] = (char *)malloc(sizeof(char) * (len + 1));
		envp[i] = ft_strjoin(ft_strjoin(elem->path, "="), elem->envp);
		i++;
		elem = elem->next;
	}
	envp[i] = NULL;
	return (envp);
}

void	ft_exec(char **av, char *s, t_env *env)
{
	char		**envp;
	struct stat	st;

	envp = ft_chg_envp(env);
	while (1)
	{
		if (lstat(av[0], &st) == 0 && st.st_mode & S_IXUSR)
			execve(av[0], av, envp);
		if (lstat(SAV, &st) == 0 && st.st_mode & S_IXUSR)
			execve(SAV, av, envp);
		s = ft_strchr_post(s, ':');
		if (!ft_strchr_post(s, ':'))
		{
			if (lstat(av[0], &st) == 0 && st.st_mode & S_IXUSR)
				ft_putstr_fd("minishell: exec format error: ", 2);
			else if (lstat(av[0], &st) == 0)
				ft_putstr_fd("minishell: permission denied: ", 2);
			else if (!ft_strchr(av[0], '/'))
				ft_putstr_fd("minishell: command not found: ", 2);
			else
				ft_putstr_fd("minishell: no such file or directory: ", 2);
			ft_putendl_fd(av[0], 2);
		}
	}
}
