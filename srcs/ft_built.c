/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/28 19:58:53 by alallema          #+#    #+#             */
/*   Updated: 2016/09/21 18:17:57 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static void	ft_unsetenv2(t_env *elem, t_env **env, char *arg)
{
	while (elem)
	{
		if (elem && ft_strcmp(arg, elem->path) == 0)
		{
			if (elem->next)
			{
				elem = elem->next;
				*env = elem;
			}
			else
				*env = NULL;
		}
		if (elem && elem->next && ft_strcmp(arg, elem->next->path) == 0)
		{
			ft_clear_elem(elem);
			break ;
		}
		elem = elem->next;
	}
	return ;
}

static int	ft_unsetenv(t_env **env, char **arg)
{
	t_env	*elem;
	int		i;

	i = 0;
	elem = *env;
	if (!arg[1])
	{
		ft_putendl("unset: not enough arguments");
		ft_set_ret(elem, 1);
		return (0);
	}
	while (arg[i] && (elem = *env))
	{
		ft_unsetenv2(elem, env, arg[i]);
		i++;
	}
	ft_set_ret(*env, 0);
	return (0);
}

int			ft_check_built(t_env **env, char **arg)
{
	int		i;

	i = 0;
	if (!arg)
		return (0);
	while (arg[i])
	{
		if (ft_strcmp("exit", arg[i]) == 0 || ft_strcmp("quit", arg[i]) == 0)
			exit(0);
		if (ft_strcmp("cd", arg[0]) == 0)
		{
			ft_chg_dir(*env, arg);
			return (0);
		}
		if (ENV && i == 0 && ft_put_env(*env, arg) == 0)
			return (0);
		if ((SET || EXP) && ft_setenv(env, arg) == 0)
			return (0);
		if ((UN || UNSET) && ft_unsetenv(env, arg) == 0)
			return (0);
		if (ft_strcmp("echo", arg[0]) == 0 && ft_echo(*env, arg) == 0)
			return (0);
		i++;
	}
	return (-1);
}
