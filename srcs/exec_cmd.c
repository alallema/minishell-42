/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/04 15:37:58 by alallema          #+#    #+#             */
/*   Updated: 2016/09/21 16:27:31 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static char	*ft_get_path(t_env *env)
{
	t_env	*elem;

	elem = env;
	while (elem)
	{
		if (ft_strcmp("PATH", elem->path) == 0)
			return (elem->envp);
		elem = elem->next;
	}
	return (NULL);
}

static int	ft_shlvl(t_env *env, int i)
{
	t_env	*elem;
	int		lvl;

	elem = env;
	while (elem && elem->next)
	{
		if (ft_strcmp("SHLVL", elem->path) == 0)
		{
			lvl = ft_atoi(elem->envp);
			if (i == 1)
				lvl++;
			else
				lvl--;
			free(elem->envp);
			elem->envp = ft_itoa(lvl);
		}
		elem = elem->next;
	}
	if (i == 1)
		return (1);
	else
		return (0);
}

void		ft_set_ret(t_env *env, int stat)
{
	char	*s;
	char	**tab;

	s = ft_join("export ?=", ft_itoa(stat));
	tab = ft_strsplit(s, ' ');
	ft_setenv(&env, tab);
	if (tab)
	{
		stat = 0;
		while (tab[stat])
		{
			ft_memdel((void *)&tab[stat]);
			stat++;
		}
		if (tab)
			free(tab);
		tab = NULL;
	}
	if (s)
		free(s);
	return ;
}

static void	ft_waitchild(t_arg *elem, t_env *env, char *s)
{
	pid_t	pid;
	int		status;
	int		i;
	int		statval;

	i = 0;
	pid = fork();
	if (elem->av[0] && env && (ft_strcmp("minishell", elem->av[0]) == 0 || \
		ft_strcmp("./minishell", elem->av[0]) == 0))
		i = ft_shlvl(env, 1);
	if (pid < 0)
		return ;
	if (pid > 0)
	{
		wait(&statval);
		if (WIFEXITED(statval))
			ft_set_ret(env, WEXITSTATUS(statval));
		if (i == 1)
			ft_shlvl(env, 0);
		waitpid(-1, &status, 0);
	}
	if (pid == 0)
		ft_exec(elem->av, s, env);
}

void		exec_cmd(t_arg *av, t_env **env)
{
	char	*s;
	t_arg	*elem;

	elem = av;
	if (env)
		s = ft_get_path(*env);
	while (elem)
	{
		if (ft_check_built(env, elem->av) < 0)
		{
			ft_waitchild(elem, *env, s);
		}
		elem = elem->next;
	}
	free(elem);
	ft_putstr("$>");
}
