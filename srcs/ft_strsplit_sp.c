/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_sp.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/02 17:31:25 by alallema          #+#    #+#             */
/*   Updated: 2016/09/14 22:01:30 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static	int		ft_count_words(char *s)
{
	int		word;
	int		i;

	i = 0;
	word = 0;
	if (!s)
		return (0);
	while (s[i])
	{
		if ((s[i] == ' ' || s[i] == '\t')
			&& s[i + 1] != ' ' && s[i + 1] != '\t')
			word++;
		i++;
	}
	if (s[0] != '\0')
		word++;
	return (word);
}

static	char	*ft_word(char *str, int *i)
{
	char	*s;
	int		k;

	if (!(s = (char *)malloc(sizeof(s) * (ft_strlen(str)))))
		return (NULL);
	k = 0;
	while (str[*i] != ' ' && str[*i] != '\t' && str[*i])
	{
		s[k] = str[*i];
		k++;
		*i += 1;
	}
	s[k] = '\0';
	while ((str[*i] == ' ' || str[*i] == '\t') && str[*i])
		*i += 1;
	return (s);
}

char			**ft_strsplit_sp(char *str)
{
	int		i;
	int		j;
	int		wrd;
	char	**s;

	i = 0;
	j = 0;
	wrd = ft_count_words(str);
	if (!(s = (char **)malloc(sizeof(s) * (wrd + 2))))
		return (NULL);
	while ((str[i] == ' ' || str[i] == '\t') && str[i])
		i++;
	while (j < wrd && str[i])
	{
		s[j] = ft_word(str, &i);
		j++;
	}
	s[j] = NULL;
	return (s);
}
