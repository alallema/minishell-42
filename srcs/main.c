/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/28 15:10:29 by alallema          #+#    #+#             */
/*   Updated: 2016/10/13 19:38:51 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include <stdio.h>

static void		get_sigint(int sig)
{
	int		status;

	waitpid(-1, &status, 0);
	if (WTERMSIG(status) == SIGINT)
		return ;
	if (sig == SIGINT && !WIFSIGNALED(status))
		return ;
	else if (sig == SIGINT)
		ft_putstr("\n$>");
	return ;
}

static t_env	*ft_creat_env(char **envp)
{
	char	buf[256];
	t_env	*s;
	int		i;

	i = 1;
	s = ft_lst_create(envp[0]);
	if (!s)
		return (ft_lst_create(getcwd(buf, 256)));
	while (envp[i])
	{
		ft_lstpushback(&s, envp[i]);
		i++;
	}
	return (s);
}

int				main(int ac, char **av, char **envp)
{
	t_env	*env;
	char	*line;
	t_arg	*arg;

	(void)av;
	env = NULL;
	if (!env)
		env = ft_creat_env(envp);
	if (ac == 0)
		return (0);
	ft_putstr("$>");
	signal(SIGINT, get_sigint);
	while (get_next_line(0, &line, '\n') > 0)
	{
		exec_cmd(arg = ft_splitcmd(line), &env);
		free(line);
		ft_clearg(arg);
	}
	free(line);
	return (0);
}
