/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chg_dir.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/28 19:58:53 by alallema          #+#    #+#             */
/*   Updated: 2016/09/17 16:41:16 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

static int	chk_dir(char **arg, char *dir)
{
	DIR		*dp;

	if (!(dp = opendir(dir)))
	{
		if (access(dir, F_OK))
		{
			ft_putstr_fd("cd: no such file or directory: ", 2);
			ft_putendl_fd(arg[1], 2);
			return (-1);
		}
		else
		{
			ft_putstr_fd("cd: permission denied: ", 2);
			ft_putendl_fd(arg[1], 2);
			return (-1);
		}
	}
	closedir(dp);
	if (chdir(dir) == -1)
		return (-1);
	return (0);
}

void		ft_chg2(t_env *elem, char **arg, char *dir, char *dir2)
{
	while (elem && ft_strcmp("PWD", elem->path) != 0)
		elem = elem->next;
	if (arg[1][0] == '/')
		chk_dir(arg, arg[1]);
	else if (elem && dir)
		chk_dir(arg, (dir2 = ft_join(dir, ft_strjoin("/", arg[1]))));
	else if ((arg[1][0] != '/') && elem)
		chk_dir(arg, (dir2 = ft_join(elem->envp, ft_strjoin("/", arg[1]))));
	else if (!elem && dir)
		chk_dir(arg, (dir2 = ft_join(dir, ft_strjoin("/", arg[1]))));
	free(dir2);
	return ;
}

void		ft_chg3(t_env *env, t_env *elem, char *s, char *dir)
{
	while (elem && ft_strcmp(s, elem->path) != 0)
		elem = elem->next;
	if (elem && ft_strcmp(s, elem->path) == 0)
		chdir(elem->envp);
	if (elem && elem->envp)
		ft_chge_env(env, elem->envp, dir);
	return ;
}

void		ft_chg4(t_env *env, char **arg)
{
	ft_putstr_fd("cd: string not in pwd:", 2);
	ft_putendl_fd(arg[1], 2);
	ft_set_ret(env, 1);
}

void		ft_chg_dir(t_env *env, char **arg)
{
	t_env	*elem;
	char	buf[256];
	char	*dir;

	ft_bzero(buf, 256);
	elem = env;
	dir = NULL;
	if (getcwd(buf, 256))
		dir = ft_strdup(getcwd(buf, 256));
	if (arg[1] && arg[1][0] == '-' && !arg[1][1])
		ft_chg3(env, elem, "OLDPWD", dir);
	else if (arg[1] && ft_strcmp(arg[1], "--") != 0 && !arg[2])
	{
		ft_chg2(elem, arg, dir, NULL);
		ft_chge_env(env, arg[1], dir);
	}
	else if (!arg[1] || ft_strcmp(arg[1], "--") == 0)
	{
		elem = env;
		ft_chg3(env, elem, "HOME", dir);
	}
	else if (arg[1] && arg[2])
		ft_chg4(env, arg);
	free(dir);
	return ;
}
