/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_echo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/13 13:49:44 by alallema          #+#    #+#             */
/*   Updated: 2016/09/21 18:17:03 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int				ft_echo(t_env *elem, char **arg)
{
	int		i;
	int		j;
	t_env	*env;

	env = elem;
	i = 0;
	j = 0;
	while (arg[++i])
	{
		if ((ft_strcmp(arg[1], "-n") == 0 && i > 2) || ECHON)
			ft_putchar(' ');
		if (arg[i][0] == '$' && arg[i][1])
		{
			while (env && ft_strcmp(env->path, &arg[i][1]) != 0)
				env = env->next;
			(env) ? ft_putstr(env->envp) : ft_putstr("");
		}
		else if (ft_strcmp(arg[i], "-n") == 0 && i == 1)
			j = 1;
		else
			ft_putstr(arg[i]);
	}
	(j != 1) ? ft_putchar('\n') : ft_putstr("");
	ft_set_ret(elem, 0);
	return (0);
}

static void		ft_put_var(t_env *env, char **arg)
{
	t_env	*elem;
	char	*s;

	s = NULL;
	while (*++arg && (elem = env))
	{
		while (elem)
		{
			if (**arg == '$')
				s = ft_strsub(*arg, 1, ft_strlen(*arg));
			if (s && ft_strcmp(s, elem->path) == 0)
			{
				ft_putstr("env: ");
				ft_putstr(elem->envp);
				ft_putendl(": No such file or directory");
				ft_memdel((void *)&s);
				return ;
			}
			if (s)
				ft_memdel((void *)&s);
			elem = elem->next;
		}
	}
	return ;
}

static int		ft_put_arg(int i, char **arg)
{
	if (ENV && arg[1])
	{
		while (*++arg && ft_strchr(*arg, '='))
		{
			if (i == 0)
				ft_putendl(*arg);
		}
		if (*arg && i < 0)
			return (-1);
	}
	return (0);
}

static int		ft_check_arg(t_env *env, char **arg)
{
	int		i;
	int		j;

	i = 1;
	j = 0;
	while (arg[i])
	{
		if (arg[i][0] == '$')
			j = 1;
		if (!ft_strchr(arg[i], '=') && j == 0)
			return (-1);
		i++;
	}
	if (j == 1)
		ft_put_var(env, arg);
	return (0);
}

int				ft_put_env(t_env *env, char **arg)
{
	t_env	*elem;
	int		i;

	i = ft_check_arg(env, arg);
	elem = env;
	while (elem && i == 0)
	{
		if (!arg[1] || ft_strchr(arg[1], '='))
		{
			if (ft_strcmp(elem->path, "?") != 0)
			{
				ft_putstr(elem->path);
				ft_putchar('=');
				ft_putendl(elem->envp);
			}
		}
		elem = elem->next;
	}
	if (ft_put_arg(i, arg) < 0)
		return (-1);
	ft_set_ret(env, 0);
	return (0);
}
