/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_post.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/01 17:34:19 by alallema          #+#    #+#             */
/*   Updated: 2016/09/14 20:01:03 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char		*ft_strchr_post(const char *s, int c)
{
	int		i;
	char	*s2;

	i = 0;
	while (s[i] != (const char)c && s[i])
		i++;
	if (s[i] == (char)c && s[i])
	{
		i++;
		if (s[i] == '\0')
			return (NULL);
		s2 = ft_strsub(s, i, ft_strlen(s) - i);
		return ((char *)s2);
	}
	return (NULL);
}
