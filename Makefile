
NAME = 21sh

IDIR = ./incs/
INCS = libft.h	\
	   ft.h
INCC = $(addprefix $(IDIR), $(INCS))

LDIR = ./libft
LIBS = -lft

SDIR = ./srcs/
SRCS = main.c \
	   ft_lst_create.c	\
	   exec_cmd.c		\
	   ft_built.c		\
	   ft_setenv.c		\
	   ft_chg_dir.c		\
	   ft_chg_env.c		\
	   ft_strsplit_sp.c	\
	   ft_lstarg.c		\
	   ft_echo.c		\
	   ft_fix.c			\
	   ft_exec.c

SRCC = $(addprefix $(SDIR),$(SRCS))

ODIR = ./objs/
OBJS = $(SRCS:.c=.o)
OBCC = $(addprefix $(ODIR),$(OBJS))

FLAG = -g -Wall -Werror -Wextra

$(NAME): $(OBCC)
	make -C ./libft/
	gcc $(FLAG) $(OBCC) -L$(LDIR) $(LIBS) -o $(NAME)

$(ODIR)%.o: $(SDIR)%.c
	@mkdir -p $(ODIR)
	gcc $^ $(FLAG) -c -o $@ -I$(IDIR)

all: $(NAME)

clean:
	@make -C ./libft/ clean
	/bin/rm -rf $(ODIR)

fclean: clean
	@make -C ./libft/ fclean
	/bin/rm -f $(NAME)

re: fclean all
